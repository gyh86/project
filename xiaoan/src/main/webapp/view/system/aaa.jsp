<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="col-sm-3">
		<h2>纵向列表</h2>
		<small>纵向列表的副标题</small>
		<ul class="list-group clear-list m-t">
			<li class="list-group-item fist-item">
				<span class="pull-right">9:00 pm</span>
				<span class="label label-success">1</span>
				 第一条数据
			</li>
			<li class="list-group-item">
				<span class="pull-right">11:00 am</span>
				<span class="label label-info">2</span>
				第二条数据
			</li>
			<li class="list-group-item">
				<span class="pull-right">12:30 pm</span>
				<span class="label label-primary">3</span>
				第三条数据
			</li>
		</ul>
	</div>
	<div class="col-sm-6">
		<div class="flot-chart dashboard-chart">
                     <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                 </div>
                 <div class="row text-left">
                     <div class="col-xs-4">
                         <div class=" m-l-md">
                         <span class="h4 font-bold m-t block">$ 406,100</span>
                         <small class="text-muted m-b block">Sales marketing report</small>
                         </div>
                     </div>
                     <div class="col-xs-4">
                         <span class="h4 font-bold m-t block">$ 150,401</span>
                         <small class="text-muted m-b block">Annual sales revenue</small>
                     </div>
                     <div class="col-xs-4">
                         <span class="h4 font-bold m-t block">$ 16,822</span>
                         <small class="text-muted m-b block">Half-year revenue margin</small>
                     </div>

                 </div>
	</div>
	<div class="col-sm-3">
		sdsa
	</div>
</body>
</html>