<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav class="navbar navbar-static-top" style="border-bottom:0px;margin-bottom: 0px;">
	<div class="navbar-header">
		<a class="navbar-minimalize minimalize-styl-2 btn btn-primary  " href="#"><i class="fa fa-bars"></i> </a>
	</div>
	<ul class="nav navbar-top-links navbar-right">
		<li class="pull-right">
			<a href="/logout"><i class="fa fa-sign-out"></i>退出</a>
		</li>
	</ul>
</nav>