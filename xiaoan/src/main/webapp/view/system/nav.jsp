<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="contextPath" value="${pageContext.request.contextPath }"></c:set>
<!----------------------左侧菜单栏  start----------------------------------->
<nav class="navbar-default navbar-static-side">
	<!-- id=‘side-menu’点击li菜单-->
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<!---->
				<div class="dropdown profile-element">
					<span><img alt="image" class="img-circle" src="${contextPath }/images/profile_small.jpg" /></span>
					<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="clear"><span class="block m-t-xs">
					<strong class="font-bold">${userName }</strong></span>
						<span class="text-muted text-xs block">操作员<b class="caret"></b></span></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">退出操作</a></li>
						<li><a href="#">记录操作</a></li>
					</ul>
				</div>
				<div class="logo-element" style="background-color: #676a6c;">IN+</div>
			</li>
			<%-- 动态生成菜单列表  start--%>
			<c:forEach var="item" items="${data }">
				<li> <a href="${item.url }"><i class="fa ${item.icon }"></i> <span class="nav-label">${item.name }</span> 
					<c:if test="${fn:length(item.children) gt 0}"><span class="fa arrow"></span></c:if></a>
					
					<c:if test="${fn:length(item.children) gt 0}">
						<ul class="nav nav-second-level collapse">
							<c:forEach var="node" items="${item.children }">
								<li><a href="${node.url }">${node.name }</a></li>
							</c:forEach>
						</ul>
					</c:if>
				</li>
			</c:forEach>
			<%-- 动态生成菜单列表  end--%>
			
			<li><a href="#"><i class="fa fa-bars"></i><span class="nav-label">人员分配</span><span class="label pull-right label-primary">NEW</span></a></li>
			<li> <a href="#"><i class="fa fa-bar-chart"></i> <span class="nav-label">数据管理</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="#">数据设置</a></li>
					<li><a href="#">数据设置</a></li>
					<li><a href="#">数据设置</a></li>
				</ul>
			</li>
			
		</ul>
	</div>
</nav>
<!----------------------左侧菜单栏  end----------------------------------->
