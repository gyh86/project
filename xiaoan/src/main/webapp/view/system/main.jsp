<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<title>系统管理主页</title>
	<!--相应式开发使用的bootstrap样式-->
	<link rel="stylesheet" href="${contextPath }/css/bootstrap.min.css" />
	<!--页面中的整体布局-->
	<link rel="stylesheet" href="${contextPath }/css/style.css" />
	<!-- 图标，左侧菜单栏中的选项前面的小图标 -->
	<link rel="stylesheet" href="${contextPath }/css/font-awesome/css/font-awesome.min.css" />
	
	<script type="text/javascript" src="${contextPath }/js/jquery-2.1.1.js" ></script>
    <script type="text/javascript" src="${contextPath }/js/bootstrap.min.js" ></script>
    <!--  -->
    <script type="text/javascript" src="${contextPath }/js/inspinia.js" ></script>
    <script type="text/javascript" src="${contextPath }/js/jquery.metisMenu.js" ></script>
    <script type="text/javascript" src="${contextPath }/js/jquery.slimscroll.min.js" ></script>
    <!--  -->
    <script type="text/javascript" src="WebContent/js/tabs/bootstrap.addtabs.min.js" ></script>
    <script type="text/javascript" src="WebContent/js/dataTables/jquery.dataTables.min.js"></script>
    
	<!-- Flot -->
    <script src="${contextPath }/js/flot/jquery.flot.js"></script>
    <script src="${contextPath }/js/flot/jquery.flot.tooltip.min.js"></script>
    <script src="${contextPath }/js/flot/jquery.flot.spline.js"></script>
    <script src="${contextPath }/js/flot/jquery.flot.resize.js"></script>
    <script src="${contextPath }/js/flot/jquery.flot.pie.js"></script>

</head>
<body>
<div id="wrapper" >
	<!----------------------左侧菜单栏 ----------------------------------->
	<jsp:include page="nav.jsp"></jsp:include>
	<!----------------------右侧内容区域 start----------------------------------->
	<div id="page-wrapper" class="gray-bg">
		<!--------------------内容区域中的导航栏 start------------------->
		<div class="row border-bottom">
			<jsp:include page="header.jsp"></jsp:include>
		</div>
		<!--------------------内容区域中的导航栏 end------------------->
		<!--------------------内容区域中的内容块 start------------------->
		<div id="content_box" class="row border-bottom white-bg dashboard-header tab-content">
			<div id="box" class="tab-pane fade">
				<jsp:include page="aaa.jsp"/>
			</div>
			<div id="box1" class="tab-pane fade">
				<jsp:include page="da.jsp"/>
			</div>
			<!-- <div id="tabs" class="tab-pane fade">
				Nav tabs
				<ul class="nav nav-tabs" role="tablist"></ul>
				<div class="wrapper wrapper-content tab-content" style="padding:10px 0"></div>
			</div> -->
		</div>	
</div>
<script type="text/javascript">
function jump(id){

}
$(function(){
	var data1 = [
        [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
    ];
    var data2 = [
        [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
    ];
    $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
        data1, data2
    ],
            {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#d5d5d5'
                },
                colors: ["#1ab394", "#1C84C6"],
                xaxis:{
                },
                yaxis: {
                    ticks: 4
                },
                tooltip: false
            }
    );


});

</script>
</body>
</html>