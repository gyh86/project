<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>用户登入</title>
<c:set var="contextPath" value="${request.getContextPath()}"></c:set>
<link rel="stylesheet" type="text/css" href="${contextPath}/css/bootstrap.min.css"/>
</head>
<body>	
<div id="box">
	<div class="col-sm-12" style="position: absolute;top: 30%;right: 20%;width: 300px;text-align: center;">
		<form class="form-horizontal" action="/login" role="form" method="post">
			<label class="control-label" style="color: red;height: 25px;">${errorInfo }</label>
			<div class="form-group">
				<label for="firstname" class="col-sm-3 control-label">用户名</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="userName" name="userName"
						   placeholder="请输入用户名">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-3 control-label">密码</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="lastname" name="password"
						   placeholder="请输入密码">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">登录</button>
				</div>
			</div>
		</form>
	</div>
</div>
</body>
</html>