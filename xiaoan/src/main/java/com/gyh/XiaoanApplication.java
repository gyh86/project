package com.gyh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
public class XiaoanApplication {

	public static void main(String[] args) {
		SpringApplication.run(XiaoanApplication.class, args);
	}
	
}
