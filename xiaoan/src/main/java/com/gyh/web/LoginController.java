package com.gyh.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gyh.config.Commons;
import com.gyh.entity.Permission;
import com.gyh.service.UserService;
import com.gyh.tools.ModuleTree;
import com.gyh.tools.UITreeFactory;

@Controller
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login() {
		return "redirect:/login.jsp";
	}
	
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public void login(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("userName");
		logger.info(userName+"用户，正在登入操作。。。。。。。。。");
		String password = request.getParameter("password");
		Map<String,Object> result = new HashMap<String,Object>();
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
		try {
			subject.login(token);
			Session session = subject.getSession();
			result.put(Commons.RESULT_MESSAGE, "登入成功");
			session.setAttribute(Commons.USER_NAME, userName);
			response.sendRedirect("/view/system/main");
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("登入失败！！！！！！！！");
			request.setAttribute("errorInfo", "用户名或者密码错误");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
			return ;
		}
	}
	
	
	@RequestMapping("/view/system/main")
	public void gainUserInfo(HttpServletRequest request,HttpServletResponse response){
		try {
			Subject subject = SecurityUtils.getSubject();
			Session session = subject.getSession();
			String userName = (String) session.getAttribute(Commons.USER_NAME);
			Set<Permission> pers = userService.gainPermissions(userName);
			List<ModuleTree> list = UITreeFactory.toListTree(pers);
			request.setAttribute(Commons.RESULT_DATA, list);
			request.getRequestDispatcher("/view/system/main.jsp").forward(request, response);
			return ;
		} catch (ServletException | IOException e) {
			logger.info("获取去操作树时，发生错误");
		}
	}
	
	
	
	@RequestMapping("/logout")
	public String logout() {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:/login.jsp";
	}
	
}
