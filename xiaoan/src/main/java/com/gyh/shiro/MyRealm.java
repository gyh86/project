package com.gyh.shiro;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import com.gyh.entity.User;
import com.gyh.service.UserService;

@Component("myRealm")
public class MyRealm extends AuthorizingRealm{
	
	private static final Logger logger = LoggerFactory.getLogger(MyRealm.class);

	@Resource 
	private UserService userService;
	
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		//获取用户名
		String userName = (String) principals.getPrimaryPrincipal();
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		authorizationInfo.setRoles(userService.sreachRoles(userName));
		authorizationInfo.setStringPermissions(userService.searchPermissions(userName));
		return authorizationInfo;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		//获取用户名字
		String userName = (String) token.getPrincipal();
		logger.info(userName+"用户，正在进行密码认证！！！！");
		User user = userService.userByuserName(userName);
		if (user == null) {
			throw new UnknownAccountException("用户名不存在！！！");
		}
		SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(token.getCredentials(), user.getPassword(),"myRealm");
		return authenticationInfo;
	}

}
