package com.gyh.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gyh.dao.UserDao;
import com.gyh.entity.Permission;
import com.gyh.entity.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired UserDao userDao;
	
	@Override
	public List<User> UserInfo() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	@Override
	public User userByuserName(String name) {
		// TODO Auto-generated method stub
		return userDao.login(name);
	}

	@Override
	public Set<String> sreachRoles(String name) {
		// TODO Auto-generated method stub
		return userDao.sreachRoles(name);
	}

	@Override
	public Set<String> searchPermissions(String userName) {
		// TODO Auto-generated method stub
		return userDao.searchPermissions(userName);
	}

	@Override
	public List<String> searchUrls(String userName) {
		List<String> list = new ArrayList<String>(userDao.searchUrls(userName));
		return list;
	}

	@Override
	public Set<Permission> gainPermissions(String userName) {
		// TODO Auto-generated method stub
		return userDao.gainPermissions(userName);
	}
	
	
	

}
