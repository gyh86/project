package com.gyh.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.query.Param;

import com.gyh.entity.Permission;
import com.gyh.entity.User;

public interface UserService {

	//获取用户信息列表
	public List<User> UserInfo();
	
	//根据用户名获取用户数据
	public User userByuserName(String name);
	
	//根据用户名获取用户所属的角色
	public Set<String> sreachRoles(String name);
	
	//根据用户名称获取用户所拥有的权限
	public Set<String> searchPermissions(String userName);
	
	//根据用户名获取用户所能访问的URL
	public List<String> searchUrls(String userName);
	
	//根据用户名获取的菜单
	public Set<Permission> gainPermissions(String userName);
	
	
}
