package com.gyh.tools;

import java.util.List;

public class ModuleTree {

	private Long id;
	
	private String name;
	
	private Long parentId;
	
	private int order;
	
	private String icon;
	
	private String url;
	
	private List<ModuleTree> children;

	
	
	public ModuleTree() {
		super();
	}

	

	public ModuleTree(Long id, String name, Long parentId, int order, String icon, String url) {
		super();
		this.id = id;
		this.name = name;
		this.parentId = parentId;
		this.order = order;
		this.icon = icon;
		this.url = url;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<ModuleTree> getChildren() {
		return children;
	}

	public void setChildren(List<ModuleTree> children) {
		this.children = children;
	}

	
	
	
	
}
