package com.gyh.tools;

import java.util.Comparator;

public class TreeComparator implements Comparator<ModuleTree>{

	/**
	 * 对数据进行排序
	 */
	@Override
	public int compare(ModuleTree o1, ModuleTree o2) {
		if((o2.getOrder()-o1.getOrder())!=0){
			return o2.getOrder()-o1.getOrder();
		}else{
			return (int) (o1.getId()-o2.getId());
		}
	}

}
