package com.gyh.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gyh.entity.Permission;
import com.gyh.entity.User;

public interface UserDao extends JpaRepository<User, Long>{

	@Query("from User where name=:name")
	User login(@Param("name") String name);
	
	/**
	 * 根据用户名，获取用户的所有的权限
	 */
	@Query("select r.name from Role r where r.id in (select ur.roleId from UserAndRole ur where ur.userId in (select u.id from User u where u.name=:userName))")
	Set<String> sreachRoles(@Param("userName")String userName);
	
	@Query("select p.name from Permission p where p.id in (select rp.perId from RoleAndPermission rp where rp.roleId in (select ur.roleId from UserAndRole ur where ur.userId in (select u.id from User u where u.name=:userName)))")
	Set<String> searchPermissions(@Param("userName")String userName);
	
	@Query("select p.url from Permission p where p.id in (select rp.perId from RoleAndPermission rp where rp.roleId in (select ur.roleId from UserAndRole ur where ur.userId in (select u.id from User u where u.name=:userName)))")
	Set<String> searchUrls(@Param("userName")String userName);
	
	@Query("from Permission p where p.id in (select rp.perId from RoleAndPermission rp where rp.roleId in (select ur.roleId from UserAndRole ur where ur.userId in (select u.id from User u where u.name=:userName))) order by p.id,p.order")
	Set<Permission> gainPermissions(@Param("userName") String userName);
	
 }
